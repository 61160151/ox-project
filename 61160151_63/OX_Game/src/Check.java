
public class Check  {
	
	Board board = new Board();
	public Check() {
		
	}
	
	public boolean checkInput(int input, String rowCol) {
    	if (input != 1 && input != 2 && input != 3) {
            System.out.println(rowCol + " Error, Please enter new information.");
            return true;
        } else {
            return false;
        }
    }
	
	public boolean checkRepeat(int row, int col) {
        if (board.board[row - 1][col - 1] == '-') {
            return false;
        } else {
            System.out.println("This row :" + row + " column :" + col + " " + "Has been used");
            return true;
        }
    }
}
