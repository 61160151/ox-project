import java.util.Scanner;

public class OX_Game {

    public static void main(String[] args) {
        System.out.println("Welcome to OXGame");
        Board table= new Board();
        Turn turn = new Turn();
        table.showBoard();
        turn.switchTurn();
    }

}