import java.util.Scanner;

public class Turn {
	
	int turn;
	boolean stop;
	Check check = new Check();
	
	public Turn(){
		turn = 0;
		stop = true;
	}
	
	public void switchTurn() {
		turn++;
    	if(turn%2==1) {
    		System.out.println("Turn O");
    		System.out.print("Please input row,col");
    		inputRowCol('O');
    		stop = checkWin(turn,stop);
    	}else {
    		System.out.println("Turn X");
    		System.out.print("Please input row,col");
    		inputRowCol('X');
    		stop = checkWin(turn,stop);
    	}
    }

	public void inputRowCol(char player) {
		Scanner kb = new Scanner(System.in);
		int row = 0, col = 0;
        boolean stopCheckRepeat = true;
        while (stopCheckRepeat) {
            boolean stopRow = true, stopCol = true;
            while (stopRow || stopCol) {
                stopRow = check.checkInput(row = kb.nextInt(), "Row");
                stopCol = check.checkInput(col = kb.nextInt(), "Col");
            }
            stopCheckRepeat = check.checkRepeat(row, col);
        }
        
    }
	
	public static boolean checkWin(int turn,boolean stop) {
    	int checkWin = 0;
    	System.out.println("checkWin");
    	//checkWin += checkWinRowO();
    	//checkWin += checkWinColO();
    	//checkWin += checkWinDiagonal_O();
    	//checkWin += checkWinRowX();
    	//checkWin += checkWinColX();
    	//checkWin += checkWinDiagonal_X();
    	//checkWin += checkDraw(turn,stop,checkWin);
        //stop = showWinner(checkWin);
        return stop;
    }
}
