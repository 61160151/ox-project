
import java.util.Scanner;

public class OX_Game {

    
    static char[][] board = new char[][]{{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    

    public static void switchTurn() {
    	boolean stop = true;
    	int turn = 0;
        while (stop) {
            turn++;
            if (turn % 2 == 1) {
                System.out.println("Turn o");
                System.out.println("Please input row,col");
                inputRowCol('O');
                stop = checkWin(turn,stop);
            } else {
                System.out.println("Turn x");
                System.out.println("Please input row,col");
                inputRowCol('X');
                stop = checkWin(turn,stop);
            }
        }
    }

    public static boolean checkWin(int turn,boolean stop) {
    	int checkWin = 0;
    	checkWin += checkWinRowO();
    	checkWin += checkWinColO();
    	checkWin += checkWinDiagonal_O();
    	checkWin += checkWinRowX();
    	checkWin += checkWinColX();
    	checkWin += checkWinDiagonal_X();
    	checkWin += checkDraw(turn,stop,checkWin);
        stop = showWinner(checkWin);
        return stop;
    }

    public static boolean checkInput(int input, String rowCol) {
    	if (input != 1 && input != 2 && input != 3) {
            System.out.println(rowCol + " Error, Please enter new information.");
            return true;
        } else {
            return false;
        }
    }

    public static void inputRowCol(char player) {
    	Scanner kb = new Scanner(System.in);
    	int row = 0, col = 0;
        boolean stopCheckRepeat = true;
        while (stopCheckRepeat) {
            boolean stopRow = true, stopCol = true;
            while (stopRow || stopCol) {
                stopRow = checkInput(row = kb.nextInt(), "Row");
                stopCol = checkInput(col = kb.nextInt(), "Col");
            }
            stopCheckRepeat = checkRepeat(row, col);
        }
        board[row - 1][col - 1] = player;
    }

    public static void showBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static boolean checkRepeat(int row, int col) {
        if (board[row - 1][col - 1] == '-') {
            return false;
        } else {
            System.out.println("This row :" + row + " column :" + col + " " + "Has been used");
            return true;
        }

    }

    public static int checkWinRowO() {
        if (board[0][0] == 'O' && board[0][1] == 'O' && board[0][2] == 'O') {
            return 1;
        } else if (board[1][0] == 'O' && board[1][1] == 'O' && board[1][2] == 'O') {
            return 1;
        } else if (board[2][0] == 'O' && board[2][1] == 'O' && board[2][2] == 'O') {
            return 1;
        }
        return 0;
    }

    public static int checkWinColO() {
        if (board[0][0] == 'O' && board[1][0] == 'O' && board[2][0] == 'O') {
            return 1;
        } else if (board[0][1] == 'O' && board[1][1] == 'O' && board[2][1] == 'O') {
            return 1;
        } else if (board[0][2] == 'O' && board[1][2] == 'O' && board[2][2] == 'O') {
            return 1;
        }
        return 0;
    }

    public static int checkWinDiagonal_O() {
        if (board[0][0] == 'O' && board[1][1] == 'O' && board[2][2] == 'O') {
            return 1;
        } else if (board[0][2] == 'O' && board[1][1] == 'O' && board[2][0] == 'O') {
            return 1;
        }
        return 0;
    }

    public static int checkWinRowX() {
        if (board[0][0] == 'X' && board[0][1] == 'X' && board[0][2] == 'X') {
            return 2;
        } else if (board[1][0] == 'X' && board[1][1] == 'X' && board[1][2] == 'X') {
        	return 2;
        } else if (board[2][0] == 'X' && board[2][1] == 'X' && board[2][2] == 'X') {
        	return 2;
     }
        return 0;
    }

    public static int checkWinColX() {
        if (board[0][0] == 'X' && board[0][1] == 'X' && board[0][2] == 'X') {
        	return 2;
        } else if (board[0][1] == 'X' && board[1][1] == 'X' && board[2][1] == 'X') {
        	return 2;
        } else if (board[0][2] == 'X' && board[1][2] == 'X' && board[2][2] == 'X') {
        	return 2;
        }
        return 0;
    }

    public static int checkWinDiagonal_X() {
        if (board[0][0] == 'X' && board[1][1] == 'X' && board[2][2] == 'X') {
        	return 2;
        } else if (board[0][2] == 'X' && board[1][1] == 'X' && board[2][0] == 'X') {
        	return 2;
        }
        return 0;
    }

    public static int checkDraw(int turn,boolean stop,int checkWin) {
        if (turn == 9 && checkWin == 0) {
            stop = false;
            return 3;
        }
        return 0;
    }

    public static boolean showWinner(int checkWin) {
        showBoard();
        if (checkWin == 1) {
            System.out.println("Player O Win");
            return false;
        } else if (checkWin == 2) {
            System.out.println("Player X Win");
            return false;
        } else if (checkWin == 3) {
            System.out.println("Draw");
            return false;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println("Welcome to OXGame");
        showBoard();
        switchTurn();

    }

}
