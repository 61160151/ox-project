
public class Player {
	private int win,lose,draw;
	
	
	private char name;
    
    public Player(char name) {
        this.name = name;
    }
    public char getName() {
        return name;
    }
    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }
    
    public void win(){
        this.win++;
    }
    public void lose(){
        this.lose++;
    }
    public void draw(){
        this.draw++;
    }
}